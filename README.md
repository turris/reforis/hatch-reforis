# hatch-reforis

## Installation

To set up `hatch-reforis` for your project you'll need to configure it in your
project's `pyproject.toml` file as a `build-system` requirement:

```toml
[build-system]
requires = ["hatchling", "hatch-reforis"]
build-backend = "hatchling.build"
```
