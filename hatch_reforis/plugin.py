# SPDX-FileCopyrightText: 2024-present CZ.NIC z.s.p.o. (https://www.nic.cz/)
# SPDX-License-Identifier: GPL-3.0-or-later


import os
import shutil
import subprocess
from pathlib import Path
from typing import Any, Dict, List

from babel.messages.mofile import write_mo
from babel.messages.pofile import read_po
from hatchling.builders.hooks.plugin.interface import BuildHookInterface


class ReforisHook(BuildHookInterface):
    PLUGIN_NAME = "reforis"
    NO_JS_BUILD = os.environ.get("REFORIS_NO_JS_BUILD")
    NO_COMPILE_MESSAGES = os.environ.get("REFORIS_NO_COMPILE_MESSAGES")

    def get_module_name(self) -> str:
        return self.build_config.hook_config["reforis"].get(
            "reforis_module", self.metadata.name
        )

    def npm_install_and_build(self, path, module_name, build_data):
        # Build all data
        subprocess.run(["npm", "install", "--save-dev"], cwd=f"{path}/js", check=True)
        build_dir = path / "reforis_static" / module_name / "js"
        subprocess.run(
            ["npm", "run-script", "build", "--", "-o", f"{build_dir}/"],
            cwd=f"{path}/js",
            check=True,
        )

        # Include compiled files in the package
        force_include = build_data.get("force_include", {})
        js_files = build_dir.glob("**/*.js")
        css_files = (build_dir / ".." / "css").glob("**/*.css")
        force_include.update({p: p.relative_to(path) for p in js_files})
        force_include.update({p: p.relative_to(path) for p in css_files})

    def compile_mesages(self, path, module_name, build_data):
        # find all po files and build mo files
        mo_files = []
        for po_file in (path / module_name).glob("**/*.po"):
            mo_file = po_file.parent / (po_file.stem + ".mo")
            locale = po_file.parent.parent.name
            with po_file.open() as f:
                catalog = read_po(f, locale)
            with mo_file.open("wb") as f:
                write_mo(f, catalog)
                mo_files.append(mo_file)

        force_include = build_data.get("force_include", {})
        force_include.update({p: p.relative_to(path) for p in mo_files})
        build_data["force_include"] = force_include

    def copy_translations_from_forisjs(self, path):
        for po in path.glob(
            "js/node_modules/foris/translations/*/LC_MESSAGES/forisjs.po"
        ):
            lang = Path(po).parent.parent.name
            path_to_copy = path / f"reforis/translations/{lang}/LC_MESSAGES/forisjs.po"
            shutil.copyfile(po, path_to_copy)

    def initialize(self, version: str, build_data: Dict[str, Any]) -> None:
        path = Path(self.build_config.root)
        module_name = self.get_module_name()

        # check whether module exists
        if not (path / module_name).exists():
            raise RuntimeError(
                f"Reforis module '{module_name}' doesn't exist in project root"
            )

        if not self.NO_JS_BUILD:
            self.npm_install_and_build(path, module_name, build_data)

        if not self.NO_COMPILE_MESSAGES:
            if module_name == "reforis":
                # we need to copy transations for forisjs from npm"
                self.copy_translations_from_forisjs(path)

            self.compile_mesages(path, module_name, build_data)

        return super().initialize(version, build_data)

    def clean(self, versions: List[str]) -> None:
        return super().clean(versions)
