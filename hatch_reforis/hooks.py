# SPDX-FileCopyrightText: 2024-present CZ.NIC z.s.p.o. (https://www.nic.cz/)
# SPDX-License-Identifier: GPL-3.0-or-later

from hatchling.plugin import hookimpl

from hatch_reforis.plugin import ReforisHook


@hookimpl
def hatch_register_build_hook():
    """Get the hook implementation."""
    return ReforisHook
